-- AstroCommunity: import any community modules here
-- We import this file in `lazy_setup.lua` before the `plugins/` folder.
-- This guarantees that the specs are processed before any user plugins.

---@type LazySpec
return {
	"AstroNvim/astrocommunity",
	-- Languages
	{ import = "astrocommunity.pack.lua" },
	{ import = "astrocommunity.pack.bash" },
	{ import = "astrocommunity.pack.docker" },
	{ import = "astrocommunity.pack.json" },
	{ import = "astrocommunity.pack.markdown" },

	-- read/write files with sudo
	{ import = "astrocommunity.editing-support.suda-vim" },

	-- colorscheme
	{ import = "astrocommunity.colorscheme.catppuccin" },

	-- github copilot integration
	{ import = "astrocommunity.completion.copilot-lua" },
	{ -- further customize the options set by the community
		"zbirenbaum/copilot.lua",
		opts = {
			suggestion = {
				auto_trigger = false,
				keymap = {
					accept = "<C-l>",
					accept_word = false,
					accept_line = false,
					next = "<C-.>",
					prev = "<C-,>",
					dismiss = "<C/>",
				},
			},
		},
	},
	{ import = "astrocommunity.media.codesnap-nvim" },
	{ import = "astrocommunity.recipes.neovide" },
	{ import = "astrocommunity.recipes.telescope-lsp-mappings" },
	{ import = "astrocommunity.editing-support.nvim-regexplainer" },
	{ import = "astrocommunity.completion.cmp-nerdfont" },
}
