#!/usr/bin/env python
import json
import logging
import subprocess
from argparse import ArgumentParser

NETWORKCTL = ["/usr/bin/networkctl", "--json=short", "status"]

EMPTY = {"text": ""}

_OFFLINE_ONLINE_STATES = {"off", "offline", "", None}
_ONLINE_CARRIER_STATES = {"carrier", "degraded-carrier"}


def get_status(device: str):
    cmd = NETWORKCTL + [device]
    result = EMPTY.copy()

    ret = subprocess.run(cmd, capture_output=True)

    if ret.returncode != 0:
        return EMPTY

    devices = []
    lines = ret.stdout.decode("utf-8").split("\n")
    for line in lines:
        if line:
            try:
                devices.append(json.loads(line))
            except json.JSONDecodeError:
                return result

    status = {}
    if len(devices) == 0:
        return EMPTY
    elif len(devices) == 1:
        status = devices[0]
    else:
        for device_state in devices:
            _state = device_state.get("CarrierState", "")
            if _state not in {"off", ""}:
                status = device_state

    _online_state = status.get("OnlineState", None)
    _carrier_state = status.get("CarrierState", None)

    if (_online_state in _OFFLINE_ONLINE_STATES) and (
        _carrier_state not in _ONLINE_CARRIER_STATES
    ):
        return EMPTY

    name = status["Name"]
    kind = status.get("Type", "")

    # Ethernet
    if name.startswith("en") or name.startswith("bond"):
        result.update({"icon": "net_wired", "text": f"{name}@"})

    # Wifi
    elif name.startswith("wl"):
        ssid = status.get("SSID", "")
        result.update({"icon": "net_wireless", "text": f"{ssid}@"})

    # VPN
    elif name.startswith("tun") or kind == "wireguard":
        result.update({"icon": "net_vpn", "state": "Warning", "text": f"{name}@"})

    else:
        logging.error(f"Illegal name: {name}")
        return EMPTY

    if "Addresses" in status:
        for address in status["Addresses"]:
            if address["Family"] == 2:
                ip = ".".join(map(str, address["Address"]))
                result["text"] += f"{ip}"
                break
    else:
        return EMPTY

    return result


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument(
        "-d", "--device", required=True, help="The device pattern.", type=str
    )
    args = vars(parser.parse_args())

    output = get_status(args["device"])
    print(json.dumps(output, indent=2))
