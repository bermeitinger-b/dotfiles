# vim: set filetype=zsh
# shellcheck disable=SC1091,SC2034
# SC1091: Follow included files
# SC2034: Unused variable [oh-my-zsh needs them]
#
# move cache dir
ZSH_CACHE_DIR="${HOME}/.cache/oh-my-zsh"
if [[ ! -d "${ZSH_CACHE_DIR}" ]]; then
    mkdir "${ZSH_CACHE_DIR}"
fi

# Path to your oh-my-zsh installation.
export ZSH="/usr/share/oh-my-zsh/"
export ZSH_CUSTOM="${HOME}/.oh-my-zsh/custom"

# Uncomment the following line to use case-sensitive completion.
CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
HYPHEN_INSENSITIVE="true"

# Completions
zstyle ':omz:update' mode disabled  # disable automatic updates
# zstyle ':omz:plugins:docker' legacy-completion yes
# zstyle ':completion:*:*:docker:*' option-stacking yes
zstyle ':completion:*' rehash true                              # automatically find new executables in path 
# Speed up completions
# zstyle ':completion:*' accept-exact '*(N)'
# zstyle ':completion:*' use-cache on
# zstyle ':completion:*' cache-path ~/.cache/zsh
# zstyle ':completion:*' matcher-list \
#    'm:{[:lower:]}={[:upper:]}' \
#    '+r:|[._-]=* r:|=*' \
#    '+l:|=*'

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
HIST_FILE=~/.zsh_history  # (directory-history will overwrite this setting)
HISTSIZE=100000
HISTFILESIZE=1000000
SAVESIZE=100000
HIST_STAMPS="yyyy-mm-dd"
setopt extendedglob              # Extended globbing. Allows using regular expressions with *
setopt nocaseglob                # Case insensitive globbing
setopt rcexpandparam             # Array expension with parameters
setopt BANG_HIST                 # Treat the '!' character specially during expansion.
setopt HIST_EXPIRE_DUPS_FIRST    # Expire duplicate entries first when trimming history.
setopt HIST_IGNORE_DUPS          # Don't record an entry that was just recorded again.
setopt HIST_IGNORE_ALL_DUPS      # Delete old recorded entry if new entry is a duplicate.
setopt HIST_IGNORE_SPACE         # Don't record an entry starting with a space.
setopt HIST_REDUCE_BLANKS        # Remove superfluous blanks before recording entry.
setopt HIST_VERIFY               # Don't execute immediately upon history expansion.

# local bin is more important
PATH="${HOME}/.local/bin:${PATH}"

# no output for direnv
export DIRENV_LOG_FORMAT=

# plugins that load first
plugins=(
    "direnv"
    "docker"
    "extract"
    "fzf"
    "git"
    "gitignore"
    "history-substring-search"
    "httpie"
    "nvm"
    "per-directory-history"
    "poetry"
    "poetry-env"
    "rsync"
    "uv"
    "uv-env"
    "wd"
    "zoxide"
    "zsh-interactive-cd"
)

# Plugin settings
bindkey '^[[1;5A' history-substring-search-up
bindkey '^[[1;5B' history-substring-search-down

zstyle ':omz:plugins:nvm' autoload yes
zstyle ':omz:plugins:nvm' lazy yes

# Load oh-my-zsh
source "${ZSH}/oh-my-zsh.sh"

# Local unsynchronized (device specific) values
if [ -f "${HOME}/.zshrc.local" ]; then
    source "${HOME}/.zshrc.local"
fi

# export PATH variable, all changes must have been above
export PATH="${PATH}"

# Set completion style here, OMZ changed it
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'

# Aliases
alias download="aria2c --continue --follow-metalink=mem --auto-file-renaming=false --check-integrity=true --conditional-get=true --file-allocation=none --max-concurrent-downloads 32 --split 16 --max-connection-per-server 16 --min-split-size 2M"

alias root="run0 --pty --user=root"

alias ls="/usr/bin/eza --icons=auto"
alias l="ls -lah"
alias ll="ls -lh"
alias la="ls -laah"

alias img="kitty +kitten icat"
alias ip="ip --color=auto"
alias pkgfile="pkgfile -D /var/lib/pacman/sync"

# Filebot aliases/functions
alias fb="filebot -rename -non-strict -no-xattr --db TheMovieDB::TV --file-filter \"none{ ext =~ /nfo|txt/ }\""
alias fbe="fb --format \"{n} - {s00e00}\""
alias fbs="fb --format \"{n}/{s00e00}\""
function fbem() {
    fbe --output "$(pwd)/Media" "$@"
}
function fbsm() {
    fbs --output "$(pwd)/Media" "$@"
}

# function to copy files with progress
function cpv () {
    rsync -pogr -h -v -e /dev/null --info=progress2 "$@"
}

# function to generate simple passwords
function password () { < /dev/urandom tr -dc A-Za-z0-9 | head -c24;echo -n;}

# function to launch yazi
function yy () {
	local tmp="$(mktemp -t "yazi-cwd.XXXXXX")"
	yazi "$@" --cwd-file="$tmp"
	if cwd="$(cat -- "$tmp")" && [ -n "$cwd" ] && [ "$cwd" != "$PWD" ]; then
		cd -- "$cwd"
	fi
	rm -f -- "$tmp"
}

function 0x0(){
    local url='https://0x0.st'
    if (( $# )); then
        local file
        for file; do
            curl \
                --silent \
                --form "file=@${file}" \
                --header "X-FileName: ${file:t}" \
                "${url}"
        done
    else
        curl \
            --silent \
            --form "file=@-" \
            "${url}"
    fi
}

# function to cleanup python cache files
function pyclean () {
    find . \
        -type f -name '*.py[co]' -delete \
        -o -type d -name __pycache__ -delete
}

# kitty shell integration
if test -n "${KITTY_INSTALLATION_DIR}"; then
    export KITTY_SHELL_INTEGRATION="enabled"
    autoload -Uz -- "${KITTY_INSTALLATION_DIR}"/shell-integration/zsh/kitty-integration
    kitty-integration
    unfunction kitty-integration
fi

# custom keybindings
bindkey \^U backward-kill-line

# Other non-oh-my-zsh plugins

## zsh-autosuggestions
ZSH_AUTOSUGGEST_STRATEGY=(history completion)
source '/usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh'

## syntax highlighting
source '/usr/share/zsh/plugins/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh'

## oh-my-posh
eval "$(oh-my-posh init zsh --config ~/.config/oh-my-posh/config.toml)"

